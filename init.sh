#!/bin/bash

# install laradock
git clone https://github.com/LaraDock/laradock.git;
cp -f _/docker-compose.yml laradock/;
cp -f _/.env api/.env;

cd laradock;
docker-compose up -d api desktop mariadb elasticsearch;

echo 'Make sure docker-proxy is running!';