export default {
	methods: {
		/**
    	 * Parse a Human formatted time into a useable one.
    	 *
    	 * @param {string} time String representation of time, i.e. 1h 10m 3s.
    	 *
    	 * @return integer Seconds
    	 */
    	parseTime(time) {
    		let seconds = 0;
    		let pattern = '^'
    			// days
    			+ '(?:(\\d*(?:\\.\\d{0,8})?\\d)h[^\\s]*\\s*)?'
    			// hours
    			+ '(?:(\\d*(?:\\.\\d{0,8})?\\d)m[^\\s]*\\s*)?'
    			// seconds
    			+ '(?:(\\d*(?:\\.\\d{0,8})?\\d)[^\\s]*\\s*)?'
    			+ '$';

    		let matches = ('' + time).match(new RegExp(pattern, 'i'));

    		if (matches) {
    			seconds = (+this.formatNumber(matches[1])) * 60 * 60
    				+ (+this.formatNumber(matches[2])) * 60
    				+ (+this.formatNumber(matches[3]));
    		}

    		return seconds;
    	},

    	/**
    	 * Convert seconds to a Human readable time.
    	 *
    	 * @param {integer} time Seconds.
    	 *
    	 * @return {string}
    	 */
    	formatTime(time) {
    		let hours = Math.floor(time / 3600);
    		let minutes = Math.floor((time - (hours * 3600)) / 60);
    		let seconds = Math.floor(time - (hours * 3600) - (minutes * 60));

    		let output = '';

    		if (hours > 0) {
    			output += hours + 'h ';
    		}

    		if (minutes > 0) {
    			output += minutes + 'm ';
    		}

    		if (seconds > 0) {
    			output += seconds + 's ';
    		}

    		return output.trim() || 0;
    	},

    	/**
    	 * Zero pad formatted number.
    	 *
    	 * @param {string} number Number to format.
    	 *
    	 * @return {string}
    	 */
    	formatNumber(number) {
    		// return ('0000' + (number ? number : '00')).slice(-2);
    		return number || 0;
    	}
	}
}