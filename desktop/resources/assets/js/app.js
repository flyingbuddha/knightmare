import VueResource from 'vue-resource';
import Hal from 'halson';

import App from './components/App.vue';

// @todo make configurable
Vue.http.options.root = 'http://api.knightmare.dev';

const app = new Vue({
    el: '#app',
    components: {
    	'app': App
    }
});