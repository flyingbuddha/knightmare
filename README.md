# README #

A simple time logging tool.

### What is this repository for? ###

* A repo to learn how to use new technologies. I'm using Lumen for the API (it communicates using HAL and ApiProblem), Vue.js for the SPA (which I hope to couple with Electron later to make a desktop app) and my future intention is to use WeeX to develop a mobile app

### How do I get set up? ###

* You'll need https://github.com/flyingbuddha/docker-proxy running for virtual host management and dnsmasq setup so that it routes all *.dev traffic to your localhost
* Run init.sh, which will setup the environment by fetching laradock and copying the necessary config files. The script will setup the containers required by the project.
* Move into the laradock container and run `docker-compose exec workspace bash`
* Move into the api directory and run `composer install`
* Move into the desktop directory and run `yarn`
* Visit http://knightmare.dev for the front-end, http://desktop.api.knightmare.dev for the back.

### Who do I talk to? ###

* Mike Holloway <me@mikeholloway.co.uk>