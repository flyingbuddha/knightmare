<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
$app->get('/', function () use ($app) {

	$logs = \App\Log::all();

	foreach ($logs as $log) {
		echo $log->time . ' > ' . $log->description . '<br>';
	}

    return $app->version();
});

$app->post('/logs', function () use ($app) {
	$log = new \App\Log;

	$log->time = 1337;
	$log->description = 'Reduced the height of the hedge';

	$log->save();
});
*/

$app->get('/logs[/{id}]', [
	'as' => 'user:get',
	'uses' => 'LogController@get',
]);

$app->post('/logs/{id}', [
	'as' => 'user:create',
	'uses' => 'LogController@post',
]);