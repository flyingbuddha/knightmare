<?php

namespace App;

class Log extends BaseModel {

	protected $fillable = ["time", "description"];

	protected $dates = [];

	public static $rules = [
		// Validation rules
	];

	// Relationships

}
