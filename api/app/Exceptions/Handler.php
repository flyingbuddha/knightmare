<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

use Log;
use Crell\ApiProblem\ApiProblem;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * Describes problem with https://tools.ietf.org/html/draft-nottingham-http-problem-07
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (method_exists($e, 'getStatusCode')) {
            $code = $e->getStatusCode();
        } else {
            $code = $e->getCode();
        }

        $code = $code ?: 500;

        $problem = new ApiProblem(
            $e->getMessage() ?: 'An unknown error occurred',
            'https://' . $request->getHost() . '/errors/' . $code
        );

        $problem->setStatus($code);

        return self::renderProblem($problem);
    }

    /**
     * Renders a ApiProblem.
     *
     * @param ApiProblem $problem An object description the problem.
     *
     * @return Response
     */
    public static function renderProblem(ApiProblem $problem)
    {
        return response(
            $problem->asJson(),
            substr($problem->getType(), -3),
            ['content-type' => 'application/problem+json']
        );
    }
}
