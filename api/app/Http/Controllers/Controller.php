<?php

namespace App\Http\Controllers;

use Crell\ApiProblem\ApiProblem;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Retrieve the model used in the resource.
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    protected function getModel()
    {
        return static::MODEL;
    }

	/**
     * Reject a request.
     *
     * @param Request $request
     * @param string  $message Description of the problem.
     * @param integer $code    HTTP status.
     * @param array   $data    Additional problem details.
     *
     * @return void
     *
     * @throws Exception Described by ApiProblem
     */
    protected function reject(Request $request, $message, $code, array $data = [])
    {
        $problem = new ApiProblem(
            $message,
            'https://' . $request->getHost() . '/errors/' . $code
        );

        $problem->setStatus($code);

        if (!empty($data)) {
            // there's no native array merging functionality with arrayaccess
            foreach ($data as $key => $value) {
                $problem[$key] = $value;
            }
        }

        return \App\Exceptions\Handler::renderProblem($problem);
    }
}
