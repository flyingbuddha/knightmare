<?php

namespace App\Http\Controllers;

use App\Exceptions\Handler as ExceptionHandler;
use Crell\ApiProblem\ApiProblem;
use Illuminate\Http\Request;
use Log;
use Nocarrier\Hal;

class LogController extends Controller
{
    /**
     * Stores the model associated with this resource.
     *
     * @var string
     */
    const MODEL = \App\Log::class;

    /**
     * Retrieve a single resource.
     *
     * @param Request $request
     * @param integer $id Id of the resource.
     *
     * @return Response
     */
    public function get(Request $request, $id = null)
    {
        if (is_null($id)) {
            return $this->getAll($request);
        }

        if (!filter_var($id, FILTER_VALIDATE_INT)
            || is_null($item = $this->getModel()::find($id))
        ) {
            return $this->reject($request, 'Resource with id not found', 404);
        }

        $grouping = $this->getModel()::getTableName();
        $resource = new Hal($request->getPathInfo(), $item->toArray());

        $resource->addLink($grouping, dirname($request->getPathInfo()));

        return response($resource->asJson(), 200, ['content-type' => 'application/hal+json']);
    }

    /**
     * Create a new resource.
     *
     * @param Request $request
     * @param integer $id Id of the resource
     *
     * @return Response
     */
    public function post(Request $request, $id)
    {
        Log::debug(__METHOD__ . ':' . $id);
    }

    /**
     * Replace an existing resource.
     *
     * @param Request $request
     * @param integer $id Id of the resource
     *
     * @return Response
     */
    public function put($id)
    {}

    /**
     * Delete an existing resource.
     *
     * @param Request $request
     * @param integer $id Id of the resource
     *
     * @return Response
     */
    public function delete($id)
    {}

    /**
     * Retrieve all logs.
     *
     * @param Request $request
     *
     * @return Response
     */
    protected function getAll(Request $request)
    {
        $items = $this->getModel()::all();
        $grouping = $this->getModel()::getTableName();
        $response = new Hal($request->getPathInfo());

        foreach ($items as $item) {
            $resource = new Hal(
                $request->getPathInfo() . '/' . $item->id,
                $item->toArray()
            );

            $resource->addLink($grouping, $request->getPathInfo());
            $response->addResource(substr($grouping, 0, -1), $resource);
        }

        return response($response->asJson(), 200, ['content-type' => 'application/hal+json']);
    }
}
